#include "HX711.h"



// HX711 circuit wiring
const int LOADCELL_DOUT_PIN = 2;
const int LOADCELL_SCK_PIN = 3;
float calibration_factor = -4850;
int forceInput;
HX711 scale;
int xValue;
int yValue;
int switchValue;
const int xPin = A0;                                               
const int yPin = A1;
const int switchPin = 7;

void setup() {
  Serial.begin(9600);
  scale.begin(LOADCELL_SCK_PIN, LOADCELL_DOUT_PIN); // Assign the pins that the controller is looking for
  scale.set_scale(calibration_factor); // Set thescale from value established in here
  scale.tare(); // function, which "resets" the scale to 0
  pinMode(switchPin, INPUT);
}



void loop() {
 forceInput = int(scale.get_units()/.0293);// get to read from 0 to 1024
 Serial.println("Value after gain : " + String(forceInput));
 // Strong Values
 forceInput = map(forceInput, 0,2400, 0, 1024);
 // Medium values
 //forceInput = map(forceInput, 0,1500, 0, 1024);
 // Lesser values
 //forceInput = map(forceInput, 0,800, 0, 1024);
 Serial.println("Value mapping : " + String(forceInput));
   yValue = analogRead(yPin);
  xValue = analogRead(xPin)-512;
  switchValue = digitalRead(switchPin);
  Serial.println("X-axis: " + String(xValue));
  Serial.println("Y-axis: " + String(yValue));
  Serial.println("Switch: " + String(switchValue));

  }





  
