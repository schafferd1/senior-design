// Check voltage of load cell

float voltage;
float voltagePart2;
int analogPin = A3;
int analogPinPart2 = A5;
float val = 0;
float valPart2 = 0;
float analog; 


void setup() {
  // put your setup code here, to run once:
pinMode(analogPin, INPUT);
pinMode(8, OUTPUT);
Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly: 
digitalWrite(8,HIGH);
val = analogRead(analogPin); // Comes in as a value 0-1024
valPart2 = analogRead(analogPinPart2); // Comes in as a value 0-1024
//Serial.println("Value from potentiometer is: ");
//Serial.print(val);
voltage = map(val, 0, 1028 , 0, 5.00);
voltagePart2 = map(valPart2, 0, 1028 , 0, 5.00);
Serial.println("Voltage on Green is " + String(val));
Serial.println("Voltage on White is " + String(valPart2));




}
