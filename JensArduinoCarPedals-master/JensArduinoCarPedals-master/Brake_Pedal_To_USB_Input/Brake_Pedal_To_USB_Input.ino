#include <DynamicHID.h>
#include "HX711.h"
#include <Joystick.h>

 // Create the Joystick
  Joystick_ Joystick;
int brakeValue = 0;


// Call HX711 to scale
HX711 scale;

// HX711 circuit wiring
const int LOADCELL_DOUT_PIN = 2;
const int LOADCELL_SCK_PIN = 3;
float calibration_factor = -4850;
int throttle;
int Steering;
int temp;
int xValue;
int yValue;
int switchValue;
int current_t = 0;
const int xPin = A0;                                               
const int yPin = A1;
const int switchPin = 7;

// init joystick libary
void setup() {
  Joystick.begin();
  pinMode(switchPin, INPUT);
  Serial.setTimeout(10);
  Serial.begin(9600);
  scale.begin(LOADCELL_SCK_PIN, LOADCELL_DOUT_PIN); // Assign the pins that the controller is looking for
  scale.set_scale(calibration_factor); // Set thescale from value established in here
  scale.tare(); // function, which "resets" the scale to 0
}

void loop() {
  // Setting the acceleration. Type number from 0 to  1024 in serial command line hit enter, type 1 to rest back to 0 on thrrottel
  // We can have this set for a certain ammount of time at a certain value to get us up to a certain speed as far as automating this
    temp = Serial.parseInt();
    if (temp != 0) {
      throttle = temp;
      
      if (temp == 1) {
        throttle = 0;
      }
      
      if (temp == 2) {

        throttle = 300;
        Joystick.setThrottle(throttle);

        delay (21720);
        
        throttle = 200;

        Joystick.setThrottle(throttle);
      }
    }
    
  throttle = throttle;
  Serial.println("Throttel = " + String(throttle));
  Joystick.setThrottle(throttle);
  
  // Two lines below are if we want to implement the joystick
  Steering = analogRead(xPin);
  if (Steering <560 & Steering >480) {

    Steering = 512;

  }
  Joystick.setSteering(Steering);
  Serial.println("Steering Posistion: " + String(Steering));

   // Brake
  brakeValue = int(scale.get_units()/.0293);// reads in from the amplifieir .0293 is what group found to be best to jump amplifeir to 1024 max value
  brakeValue = map(brakeValue, 0,1200, 0, 1024);// Map the value back down to somthing th Joystick library can see, the max reading recived was 2277, 2400 for safty
  if (brakeValue < 0){
    brakeValue = 0;
  }
  if (brakeValue > 1023) {
    brakeValue = 1023;
  }
  Joystick.setBrake(brakeValue);
  Serial.println("Brake = " + String(brakeValue));
  
  delay(1); 
  
}
